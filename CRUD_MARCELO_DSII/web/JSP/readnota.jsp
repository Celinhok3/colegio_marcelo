<%@page import="modeloCliente.Disciplina"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page import="modeloCliente.Aluno"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>SIGES - Professor</title>
    </head>
    <body>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../View/painelcoord.html">SIGES</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aluno
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../View/cadastrarcoord.html">Cadastramento</a>
                            <a class="dropdown-item" href="../View/escolherturma.html">Visualizar Alunos</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"></a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Boletim
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Visualizar nota</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"></a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
    <center><p class="font-weight-bold">Segue abaixo as notas do aluno desejado</p></center>
    <div id="list" class="row">    
        <div class="table-responsive col-md-12">
            <table class="table table-striped" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Disciplina</th>
                        <th>Nota</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>

                        <%

                            String idaluno = request.getParameter("widaluno");
                            String iddisciplina = request.getParameter("wdisciplina");
                            Aluno a = new Aluno();
                            Disciplina d = new Disciplina();

                            try {
                                a.setTurma(Integer.parseInt(idaluno));
                                d.setId(Integer.parseInt(iddisciplina));
                                
                                String sql = "select aluno.id, aluno.nome , disciplina.nome, boletim.nota from aluno join disciplina join boletim on (aluno.id = ?) and (disciplina.id = ?)";
                                // sql += "SELECT bol_nota1, bol_nota2, bol_mediafinal, bol_frequencia, bol_aprovado,bol_id FROM public.boletins INNER JOIN ALUNOS ON(BOLETINS.AL_ID = ALUNOS.AL_ID) INNER JOIN DISCIPLINAS ON (BOLETINS.DIS_ID = DISCIPLINAS.DIS_ID) WHERE BOLETINS.AL_ID = ?";
                                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                                // Conexao com todos os seus elementos
                                Conexion.Conectar();

                                //Statement
                                PreparedStatement stmt = Conexion.getCon().prepareStatement(sql);

                                stmt.setInt(1, a.getTurma());
                                stmt.setInt(2, d.getId());

                                ResultSet rs = stmt.executeQuery();
                                while (rs.next()) {

                                    int id = rs.getInt("aluno.id");
                                    String aluno_nome = rs.getString("aluno.nome");
                                    String disciplina = rs.getString("disciplina.nome");
                                    String nota = rs.getString("boletim.nota");

                        %>

                        <td><%out.println(id);%></td>
                        <td><%out.println(aluno_nome);%></td>
                        <td><%out.println(disciplina);%></td>
                        <td><%out.println(nota);%></td>
                    </tr>

                    <%
                            }

                            rs.close();
                            stmt.close();

                        } catch (Exception e) {
                            out.println("erro ai selecionado dados" + e);
                        }

                    %>
                </tbody>
            </table>

        </div>
    </div> <!-- /#list -->
</body>
</html>