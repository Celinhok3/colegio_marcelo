<%-- 
    Document   : index
    Created on : 21/08/2019, 09:49:54
    Author     : Marcelo Ribeiro
--%>


<%@page import="modeloCliente.Aluno"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.Types"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Consultando</title>
    </head>
    <body>

        <%
            //ConexaoBancoDeDados.Conectar();
            //out.println(ConexaoBancoDeDados.getCon());
            String nome = request.getParameter("wnome");
            String turma = request.getParameter("wturma");
            String email = request.getParameter("wemail");
            String cpf = request.getParameter("wcpf");
            String sexo = request.getParameter("wsexo");
            String responsavel = request.getParameter("wresponsavel");
            String telefone = request.getParameter("wtelefone");

            Aluno a = new Aluno();
            a.setNome(nome);
            a.setTurma(Integer.parseInt(turma));
            a.setEmail(email);
            a.setCpf(cpf);
            a.setSexo(sexo);
            a.setResponsavel(responsavel);
            a.setTelefone(telefone);

            try {
                // cria um preparedStatement
                String sql = "insert into aluno (id,nome,id_turma,email,cpf,sexo,responsavel,telefone) values (default,?,?,?,?,?,?,?)";
                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                // Conexao com todos os seus elementos
                Conexion.Conectar();

                //Statement
                PreparedStatement stmt = Conexion.getCon().prepareStatement(sql);

                //preenche os valores
                stmt.setString(1, a.getNome());
                stmt.setInt(2, a.getTurma());
                stmt.setString(3, a.getEmail());
                stmt.setString(4, a.getCpf());
                stmt.setString(5, a.getSexo());
                stmt.setString(6, a.getResponsavel());
                stmt.setString(7, a.getTelefone());
                // executa
                stmt.execute();
                stmt.close();
                response.sendRedirect("../View/painelcoord.html");
                Conexion.getCon().close();
            } catch (SQLException e) {
                out.println("Erro " + e);
            }


        %>

        
    </body>
</html>
