<%@page import="modeloCliente.Disciplina"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="conexao.ConexaoBancoDeDados"%>
<%@page import="modeloCliente.Nota"%>
<%@page import="modeloCliente.Aluno"%>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>SIGES - Professor</title>
    </head>
    <body>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">SIGES</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Aluno
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="../View/cadastrarcoord.html">Cadastramento</a>
                            <a class="dropdown-item" href="../View/escolherturma.html">Visualizar Alunos</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"></a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Boletim
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Visualizar nota</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"></a>
                        </div>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>

        <div class="container-fluid" style="width: 500px; margin-top: 150px;">
            <center><p class="font-weight-bold">Digite a nota do aluno</p></center>
            <form class="form-inline" action="lancarnota.jsp" method="post">
                <div class="form-group mx-sm-3 mb-2">
                    <label for="inputPassword2" class="sr-only">Nota</label>
                    <input type="text" class="form-control" name="nota" placeholder="Nota">
                </div>
                <button type="submit" class="btn btn-primary mb-2">Enviar para o sistema</button>
            </form>
        </div>
        
        
        
        
        
        <%
           String idaluno = request.getParameter("idaluno");
           String idturma = request.getParameter("idturma");
           String nota = request.getParameter("nota");
           String disciplina = request.getParameter("disciplina");
            Aluno a = new Aluno();
            Nota n = new Nota();
            Disciplina d = new Disciplina();
            
            a.setId(Integer.parseInt(idaluno));
            a.setTurma(Integer.parseInt(idturma));
            n.setNota(nota);
            d.setNome(disciplina);
          

            try {
                // cria um preparedStatement
                String sql = "insert into boletim (id,nota,id_aluno,id_dis) values (default,?,?,?)";
                ConexaoBancoDeDados Conexion = new ConexaoBancoDeDados();

                // Conexao com todos os seus elementos
                Conexion.Conectar();

                //Statement
                PreparedStatement stmt = Conexion.getCon().prepareStatement(sql);

                //preenche os valores
                stmt.setString(1, n.getNota());
                stmt.setInt(2, a.getId());
                stmt.setString(3, d.getNome());
                
                // executa
                stmt.execute();
                stmt.close();
                response.sendRedirect("../View/painelcoord.html");
                Conexion.getCon().close();
            } catch (SQLException e) {
                out.println("Erro " + e);
            }
           
          
        %>
    </body>
</html>