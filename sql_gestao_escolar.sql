create database if not exists sistema;

use sistema;

-- drop database sistema;

create table if not exists Login(
id int primary key auto_increment,
usuario varchar(9),
senha varchar(8),
acesso enum ('adm','prof','aluno')
);

select * from Login where id = 3;
delete from login where id =2;
select * from login;
select * from login where id = 3 and senha = 12345678;


create table if not exists Turma (
id int auto_increment primary key,
nome varchar(45),
vagas int not null,
turno enum ('Matutino','Vespertino','Vespertino') not null, 
serie int not null,
etapa enum('Infatil','Fundamental','Medio')	
);

create table if not exists Aluno(
id int not null auto_increment,
id_turma int,
nome varchar (255) not null,
cpf varchar(255),
sexo enum('M','F'),
responsavel varchar (255) not null,
responsavel01 varchar (255),
fone varchar(255),
fone01 varchar (255),
obs varchar (255),
primary key(id,id_turma),
constraint fk_aluno_turma foreign key (id_turma) references turma(id)
);


create table if not exists Disciplina (
id int auto_increment primary key,
nome varchar (255),
turno enum ('Mat','Vesp')
);

create table if not exists Professores (
id int not null auto_increment,
id_turma int,
id_dis int,
nome varchar (255) not null,
cpf varchar (255) not null,
sexo enum('M','F'),
fone varchar (255),
primary key (id,id_turma,id_dis),
constraint fk_prof_turma foreign key (id_turma) references turma(id),
constraint fk_prof_disciplina foreign key (id_dis) references disciplina(id)
);


create table if not exists Aula(
id int auto_increment,
d_aula date,
id_professor int,
id_turma int,
id_dis int,
primary key (id,id_professor,id_turma,id_dis),
constraint fk_aula_professor foreign key (id_professor) references professores(id),
constraint fk_aula_disciplina foreign key (id_dis) references disciplina(id),
constraint fk_aula_turma foreign key (id_turma) references turma(id) 
);


create table if not exists Boletim (
id int unsigned auto_increment,
nota double,
id_aluno int,
id_dis int,
primary key (id,id_aluno,id_dis),
constraint fk_boletim_aluno foreign key (id_aluno) references aluno(id),
constraint fk_boletim_disciplina foreign key (id_dis) references disciplina(id)
);