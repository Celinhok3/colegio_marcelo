use bancosiges;


create table if not exists Turma (
id int auto_increment primary key,
nome varchar(45),
vagas int not null,
turno enum ('Matutino','Vespertino','Noturno') not null, 
serie int not null,
etapa enum('Infatil','Fundamental','Medio')	
);

insert into turma values (default,'THE6111',60,'Matutino',1,'Medio');
insert into turma values (default,'OLG5677',40,'Vespertino',3,'Fundamental');
create table if not exists Aluno(
id int not null auto_increment,
nome varchar (255) not null,
id_turma int,
email varchar(100) not null,
cpf varchar(255),
sexo enum('M','F','O'),
responsavel varchar (255) not null,
telefone varchar(255),
primary key(id,id_turma),
constraint fk_aluno_turma foreign key (id_turma) references turma(id)
);


select id,nome from aluno where id_turma = 1;